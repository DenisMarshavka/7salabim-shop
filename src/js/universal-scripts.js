import $ from 'jquery';
import Swiper from "swiper";

//START: Specifications
let scroll_page_to_section_working = false;
//SLIDERS
let class_name_product_item = '.products-item';

if (window.outerWidth >= 768) {
    $(class_name_product_item + '.products-item--mobile, ' + class_name_product_item + '.products-item--extra-mobile').parent().remove();
} else if (window.outerWidth < 551) {
    $(class_name_product_item).not('.products-item--extra-mobile').parent().remove();
} else {
    $(class_name_product_item).not('.products-item--mobile').parent().remove();
}
//SLIDERS

//Scroll
$(document).on('scroll', 'body, html', function (event) {
    if (scroll_page_to_section_working) {
        event.preventDefault();
        event.stopPropagation();
    }
});
//Scroll

//Popup
changePopup();

function changePopup(is_open = true) {
    $('html, body').css((is_open) ? {overflowY: 'hidden'} : {overflowY: 'auto'});

    if (window.outerWidth <= 767) $('html, body').css((is_open) ? {position: 'fixed'} : {position: 'relative'});
}
//Popup
//END: Specifications

$(document).ready(function () {
    //START: Specifications
    //Alert Cookie
    let current_status_readed_cookie = false;
    checkApprovedCookies();

    if (current_status_readed_cookie) $('#alert-cookie').addClass('hide');

    $(document).on('click', '#js-alert-cookie-btn', function () {
        localStorage.setItem('cookie_approve', 'true');
        $(this).parents('.alert-cookie').addClass('hide');

        checkApprovedCookies();
    });

    $(window).scroll(function () {
        if (!current_status_readed_cookie) {
            if ( (window.pageYOffset + window.outerHeight) >= $('html').height()) {
                $('.alert-cookie').addClass('hide');
            } else $('.alert-cookie.hide').removeClass('hide');
        }
    });

    function checkApprovedCookies() {
        current_status_readed_cookie = localStorage.getItem('cookie_approve') !== null && localStorage.getItem('cookie_approve') === 'true';
    }
    //Alert Cookie

    //SEO optimization
    $(document).on('click', 'a', function () {
        if (window.location.pathname === $(this).attr('href')) {
            return false;
        }
    });
    //SEO optimization

    //Popup
    setTimeout(function () {
        changePopup(false);
        $('#preloader').addClass('hide');
    }, 500);
    //Popup

    //START: Percent sale
    $('.product-item').each(function () {
        let percent_sale = 0,
            old_price    = 0,
            new_price    = 0,
            html_sale    = '';

        if ($(this).find('strike').length && !$(this).find('.product-item__new').length) {
            if ( !$(this).parent().hasClass('product-item--present') ) {
                old_price = +$(this).find('strike').text();
                new_price = parseInt($(this).find('.price__number').html());

                if (old_price > new_price) {
                    percent_sale = getNumSale(old_price, new_price);

                    html_sale = "<div class='product-item__sale'><span>" + percent_sale + "</span></div>";
                    $(this).append(html_sale);
                }
            }
        }
    });

    function getNumSale(old_price = false, new_price = false) {
        let one_percent               = old_price / 100,
            percent_between_new_price = new_price / one_percent;

        return Math.round(100 - percent_between_new_price);
    }
    //END: Percent sale

    //START: Card product
    let is_card_product_page = $('.card-section-right').length;

    if (is_card_product_page) $('.js-card-product-image').eq(0).click();

    changeCountIndicatorProduct();

    function changeCountIndicatorProduct() {
        let arr_added_products  = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [],
            $product_count_calc = $('.js-count-calculator-indicator');

        if (is_card_product_page && arr_added_products.length) {
            for (let i = 0; i < arr_added_products.length; i++) {
                if (arr_added_products[i]['id'] === +$('.card-section-right').data('product-id')) {
                    $product_count_calc.addClass('hide');

                    setTimeout(function () {
                        $product_count_calc.text( (+arr_added_products[i]['count'] <= +$product_count_calc.data('max-count')) ? arr_added_products[i]['count'] : $product_count_calc.data('max-count') );
                    }, 200);

                    setTimeout(function () {
                        $product_count_calc.removeClass('hide');

                        getCheckActiveBtnBuyCard(+$product_count_calc.data('max-count'), +$('.card-section-right').data('product-id'));
                    }, 350);
                }
            }
        }
    }

    function getCheckActiveBtnBuyCard(max_count = false, product_id = false) {
        let arr_added_products = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [],
            selected_color     = '';

        if (max_count !== false && product_id !== false) {
            if ($('.card-section-right__btn.js-product-btn-buy').length && arr_added_products.length) {
                for (let e = 0; e < arr_added_products.length; e++) {
                    if (+arr_added_products[e]['id'] === +product_id) {

                        if (arr_added_products[e]['color'] !== undefined && $.trim(arr_added_products[e]['color']) !== 'default') {
                            selected_color = $.trim(arr_added_products[e]['color']);

                            if ($('.js-sort-colors-item[data-color="' + selected_color + '"]').length) {
                                $('.colors-list li.active').removeClass('active');

                                $('.js-sort-colors-item[data-color="' + selected_color + '"]').parent('li').addClass('active');
                            }
                        }

                        if (+arr_added_products[e]['count'] > +max_count) {
                            $('.card-section-right__btn.js-product-btn-buy').attr('disabled', 'disabled');
                        } else {
                            if ($('.card-section-right__btn.js-product-btn-buy').attr('disabled') !== undefined) $('.card-section-right__btn.js-product-btn-buy').attr('disabled', false);
                        }

                        break;
                    }
                }
            }
        }
    }

    $(document).on('click', '.colors-list li, .js-product-btn-buy', function () {
        let product_id = false;

        if (!$(this).hasClass('active')) {
            if ($(this).parents('.card-section-right').length) {
                product_id = +$(this).parents('.card-section-right').data('product-id');
            } else if ($(this).parents('.product-item').length) {
                product_id = +$(this).parents('.product-item').data('product-id');
            }
        }

        setTimeout(changeOrderProductColor, 1000, product_id);
    });

    function changeOrderProductColor(product_id = false) {
        let arr_added_products    = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [],
            $current_active_color = $('.colors-list li.active a.item');

        if (product_id !== false) {
            if (arr_added_products.length) {
                for (let e = 0; e < arr_added_products.length; e++) {
                    if (+arr_added_products[e]['id'] === +product_id) {
                        if ( $current_active_color.data('color') !== undefined && !$current_active_color.hasClass('item--default') ) {
                            arr_added_products[e]['color'] = $.trim( $current_active_color.data('color') );
                        } else arr_added_products[e]['color'] = 'default';
                    }
                }

                localStorage.setItem('added_products', JSON.stringify(arr_added_products));
            }
        }
    }

    //Images grid slider
    var swiperCardImages = new Swiper('#js-card-images-grid-slider', {
        slidesPerView: 3,
        spaceBetween: 15,
        direction: 'vertical',
        navigation: {
            nextEl: '#js-card-images-btn-down',
            prevEl: '#js-card-images-btn-up',
        },
        // pagination: {
            // el: '.swiper-pagination',
            // clickable: true,
        // },
    });

    // let card_grid_images_item_width = $('.card-section-left__swiper-slide').width(),
    //     card_grid_btn_icons_width   = (window.outerWidth > 767) ? $('#js-card-images-btn-up i').width() / 1.7 : $('#js-card-images-btn-up i').width() / 1.6;
    //
    // $('#js-card-images-btn-up, #js-card-images-btn-down').css( 'left', ( (card_grid_images_item_width / 2) - card_grid_btn_icons_width) );
    //Images grid slider
    //END: Card product

    //START: Scroll to these sections, of the menu links
    if (window.location.hash) pageScrollTo(window.location.hash, true);

    $(document).on('click', '.header-menu__item, .footer-menu__item, .footer-menu__item', function (event) {
        let scroll_to_section = ($(this).hasClass('header-menu__item-submenu-list') && $(this).parents('header')) ? false : (!$(this).hasClass('footer-menu__item')) ? $(this).children('a').attr('href').split('/')[1] : $(this).attr('href').split('/')[1];

        if (scroll_to_section) {
            if (scroll_to_section.indexOf('#') > -1 && $(scroll_to_section).length) {
                event.preventDefault();

                pageScrollTo(scroll_to_section);
            }
        }
    });

    function pageScrollTo(to_section = false, reset_offset_top = false) {
        if (to_section) {
            if (to_section && $(to_section).length) {
                if (reset_offset_top) $('html, body').animate({scrollTop: 0}, 0);
                scroll_page_to_section_working = true;

                setTimeout(function () {
                    $('html, body').animate({scrollTop: parseInt($(to_section).offset().top)}, 1000);

                    scroll_page_to_section_working = false;
                }, 500);
            }
        }
    }
    //END: Scroll to these sections, of the menu links

    //Forms
    if ($('input[required], textarea[required]').length) {
        $('input[required], textarea[required]').each(function () {
            if ($(this).next('span').length) {
                if ($.trim($(this).val()) === '') {
                    if ($(this).next('span').hasClass('hide')) $(this).next('span').removeClass('hide');
                } else {
                    if (!$(this).next('span').hasClass('hide')) $(this).next('span').addClass('hide');
                }
            }
        });
    }

    $(document).on('keyup mouseleave', 'input[required], textarea[required]', function () {
        if ($(this).parent().children('span').length && $.trim($(this).val()) <= 0) {
            if ($(this).attr('name').indexOf('card') <= -1)  $(this).val('');
        }

        checkInput($(this));
    });

    $(document).on('keyup', '.form-input-row__field', function () {
        let parent_list_element = [],
            all_items           = [];

        if ($(this).parents('.complete-order-content').length) {
            parent_list_element = ($(this).parents('.complete-order-content__form-section-head-center').length) ? $(this).parents('.complete-order-content__form-section-head-center') : $(this).parents('.complete-order-content__order-pay');
            all_items           = parent_list_element.children();

            for (let input of all_items) {
                if ($(input).hasClass('form-input-row') && !$(input).hasClass('small')) {
                    $(input).addClass('small');
                    $(input).find('label').addClass('hide');
                }
            }
        }


        $(this).parents('.form-input-row').removeClass('small');
        $(this).parents('.form-input-row').find('label').removeClass('hide');
        $(this).parents('.form-input-row').find('label').click();
    });

    $(document).on('click mouseleave', 'input.error, textarea.error', function () {
        let $alert_message_element = ($(this).parents('.complete-order-content__section-left').length)  ? $('.complete-order-content__section-left').find('.text-error')  :
                                     ($(this).parents('.complete-order-content__section-right').length) ? $('.complete-order-content__section-right').find('.text-error') : $(this).parents('form').find('.text-error');

            console.log($alert_message_element);

        if ($(this).hasClass('error')) $(this).removeClass('error');

        $alert_message_element.slideUp(500);

        setTimeout(function () {
            $alert_message_element.remove();
        }, 500);
    });

    $(document).on('click', '.js-complete-order-label', function () {
        let parent_for_this_label = null,
            $current_field        = null;

        if ($('.complete-order-content').length) {
            parent_for_this_label = ($(this).parents('.complete-order-content__form-section-head-center').length) ? '.complete-order-content__form-section-head-center' : '.complete-order-content__order-pay';

            $(parent_for_this_label + ' .form-input-row.small').each(function () {
                $current_field = $(this).find('input, textarea');

                $current_field.val('');

                if ($current_field.next('span').length && $current_field.next('span').hasClass('hide')) {
                    $current_field.next('span').removeClass('hide');
                }
            });
        }
    });
    
    function checkInput($this = false) {
        if ($this) {
            if ($($this).parent().children('span').length && $($this).val().length) {
                $($this).parent().children('span').addClass('hide');
            } else {
                $($this).parent().children('span').removeClass('hide');
            }
        }
    }

    //Complete order
    $('.js-complete-order-label').on('click', function () {
        let class_parent    = ( $(this).parents('.complete-order-content__form-section-head-center').length ) ? '.complete-order-content__form-section-head-center' : '.complete-order-content__order-pay',
            input_last_name = 'input[name="last-name"]';

        $(class_parent + ' ' + '.js-complete-order-label').each(function () {
            $(this).addClass('hide');

            $(this).parent().addClass('small');

            if ( $(this).next().next('span').length ) {
                if ( $(this).parent().find('input').length ) $(this).parent().find('input').removeAttr('required');
            }
        });

        $(this).removeClass('hide');

        if ( $(this).parent().find('input').length ) $(this).parent().find('input').attr('required', true);

        if ($(this).parent().hasClass('small')) {
            $(this).parent().removeClass('small');
        }

        if (class_parent === '.complete-order-content__order-pay') {
            if ($(this).attr('for') === 'complete-form-radio-card' || $(this).attr('for') === 'complete-form-radio-rec-token') {
                if ($(input_last_name).length) {
                    if ($(input_last_name).attr('required') === undefined) $(input_last_name).attr('required', true);
                    if ($(input_last_name).next('span').length && $(input_last_name).next('span').hasClass('hide') && $.trim($(input_last_name).val()) === '') $(input_last_name).next('span').removeClass('hide');
                }
            } else {
                if ($(input_last_name).length) {
                    if ($(input_last_name).attr('required') !== undefined) $(input_last_name).attr('required', false);
                    if ($(input_last_name).next('span').length && !$(input_last_name).next('span').hasClass('hide')) $(input_last_name).next('span').addClass('hide');
                }
            }
        }
    });

    $(document).on('click', '.form-input-row', function () {
        if ($(this).hasClass('small')) {
            $(this).removeClass('small').find('label').removeClass('hide');
        }
    });

    let complete_order_sum_all_products = 0;

    if ( $('.complete-order-products-list__item').length ) {
        $('.complete-order-products-list__item').each(function () {
            let current_product_count = $(this).find('.js-complete-order-product-count').text().split('x');

            complete_order_sum_all_products += +current_product_count[1] * $(this).find('.js-basket-product-price').data('price');
        });

        setTimeout(function () {
            getSumPrices(complete_order_sum_all_products);
        });
    }

    let height_products_content            = $('#js-complete-order-products-content').height(),
        $complete_product_list             = $('#js-complete-order-products-list'),
        id_name_complete_products_btn_prev = 'js-complete-order-product-btn-prev',
        id_name_complete_products_btn_next = 'js-complete-order-product-btn-next',
        height_products_list               = $complete_product_list.height(),
        difference_between_heights         = height_products_list - height_products_content,
        complete_products_item_class_name  = '.complete-order-products-list__item',
        height_slider_products_item        = $(complete_products_item_class_name).height(),
        offset_top_products_item           = parseInt($(complete_products_item_class_name + ':nth-child(2)').css('margin-top')),
        padding_bottom_products_item       = parseInt($(complete_products_item_class_name).css('padding-bottom')),
        step_slider_products               = height_slider_products_item + offset_top_products_item + padding_bottom_products_item,
        rule_to_move_products             = false;

    let products_current_position_top,
        current_position_top_numbers_list,
        rule_prev_product,
        rule_next_product;

    initializeAllSizesProductsList();

    if (height_products_list > height_products_content) {
        rule_to_move_products = true;
    } else {
        $('#' + id_name_complete_products_btn_next + ', ' + '#' + id_name_complete_products_btn_prev).attr('disabled', true);
    }

    $(document).on('click', '#' + id_name_complete_products_btn_prev + ', ' + '#' + id_name_complete_products_btn_next, function () {
        if (rule_to_move_products) {
            if ($(this).attr('id') === id_name_complete_products_btn_prev && rule_prev_product) {
                $complete_product_list.css( 'top', products_current_position_top + step_slider_products );
            } else {
                if ($(this).attr('id') === id_name_complete_products_btn_next && rule_next_product) {
                    $complete_product_list.css( 'top', products_current_position_top - step_slider_products );
                }
            }

            rule_to_move_products = false;
            setTimeout(initializeAllSizesProductsList, 400, true);
        }
    });

    function initializeAllSizesProductsList(moved_product = false) {
        products_current_position_top     = parseInt($complete_product_list.css('top')),
        current_position_top_numbers_list = products_current_position_top.toString().split('-'),
        rule_next_product                 = ( (current_position_top_numbers_list.length > 1) ? +current_position_top_numbers_list[1] : +current_position_top_numbers_list[0] ) <= difference_between_heights,
        rule_prev_product                 = ( (current_position_top_numbers_list.length > 1) ? +current_position_top_numbers_list[1] : +current_position_top_numbers_list[0] ) > 0;

        $('#' + id_name_complete_products_btn_prev).attr('disabled', !rule_prev_product);
        $('#' + id_name_complete_products_btn_next).attr('disabled', !rule_next_product);

        if (moved_product) {
            setTimeout(function () {
                rule_to_move_products = true;
            }, 250);
        }
    }
    //Complete order
    //Forms

    //Popup
    let id_popup = '#js-popup';

    $(document).on('click', '.js-popup-btn-close, #js-btn-continue-purchase, #js-alert-add-product-btn-close', function () {
        changePopup(false);
        $(this).parents(id_popup).toggleClass('hide');

        setTimeout(function () {
            $('.popup-form').addClass('hide');
            $('#js-alert-add-product').addClass('hide');
        }, 350);
    });

    $(document).on('click', '.js-open-popup-form', function () {
        changePopup();

        $(id_popup).toggleClass('hide');

        if ( $(this).attr('id') == 'js-open-popup-form-reviews' ) {
            $(id_popup).find('.popup-form--reviews').removeClass('hide');
        } else $(id_popup).find('.popup-form--questions').removeClass('hide');
    });
    //Popup
    //END: Specifications

    //START: Header Menu
    let class_name_language_item               = 'header-section-top__nav-language-item',
        class_name_language_item_active        = class_name_language_item + '--active',
        class_name_language_item_mobile        = 'header-head__language-item',
        class_name_language_item_active_mobile = class_name_language_item_mobile + '--active';

    $(document).on('click', '.header-section-top__nav-language-item, .header-head__language-item', function () {
        $('.' + class_name_language_item).removeClass(class_name_language_item_active);
        $('.' + class_name_language_item_mobile).removeClass(class_name_language_item_active_mobile);

        $('.' + class_name_language_item + ':contains('+ $(this).text() +')' ).addClass(class_name_language_item_active);
        $('.' + class_name_language_item_mobile + ':contains('+ $(this).text() +')' ).addClass(class_name_language_item_active_mobile);
    });

    //Package
    let element_products_attr              = 'span[data-products]',
        class_name_navigation__item        = 'header__section-top-right-list-item--package',
        class_name_navigation__item_mobile = 'head-nav-navigation__item--package',
        current_products                   = 0,
        length_added_products              = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [];

    if (length_added_products.length) {
        $(element_products_attr).attr('data-products', length_added_products.length);
    } else {
        $(element_products_attr).attr('data-products', 0);
    }

    current_products = +$(element_products_attr).attr('data-products');

    if (!current_products) {
        $(element_products_attr).remove();
    } else {
        $('.' + class_name_navigation__item_mobile + ' ' + element_products_attr + ', ' + '.' + class_name_navigation__item + ' ' + element_products_attr).text( (current_products < 100) ? current_products : '99+');

        changeStylePackage();
    }

    function changeStylePackage() {
        $('.' + class_name_navigation__item_mobile + ' ' + element_products_attr).css( (current_products < 10) ? {left: '-30px'} : (current_products > 99) ? {left: '-40px'} : {left: '-35px'});
    }

    $(document).on('click', '.js-product-btn-buy', function () {
        let current_product_id = ($(this).parents('.card-section-right').length) ? $(this).parents('.card-section-right').data('product-id') : $(this).parents('.product-item').data('product-id');

        changePopup();
        $('.js-popup-form').parents('#js-popup').toggleClass('hide');
        $(this).parents(id_popup).toggleClass('hide');

        $('#js-alert-add-product').removeClass('hide');

        if ( addThisProduct(current_product_id, $(this)) ) {
            let count_added_products = (current_products < 99) ? ++current_products : '99+',
                html                 = '<span data-products="1"></span>';

            if (!$('.' + class_name_navigation__item_mobile + ' ' + element_products_attr + ', ' + '.' + class_name_navigation__item + ' ' + element_products_attr).length) {
                $('.' + class_name_navigation__item_mobile + ' a' + ', ' + '.' + class_name_navigation__item + ' a').append(html);
            }

            $('.' + class_name_navigation__item_mobile + ' ' + element_products_attr + ', ' + '.' + class_name_navigation__item + ' ' + element_products_attr).text( count_added_products );

            changeStylePackage();
        }

        if ( ($('.card-section-right__btn.js-product-btn-buy').length) && ($(this).hasClass('card-section-right__btn')) ) getCheckActiveBtnBuyCard(+$('.js-count-calculator-indicator').data('max-count'), +$('.card-section-right').data('product-id'));
    });

    function addThisProduct(product_id = false, $this = false) {
        if (product_id !== false) {
            let arr_added_products    = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [],
                new_product           = {
                    'id'  : +product_id,
                   'count':  1
                },
                flag_change_nmb_added = true,
                flag_calc_count     = false;

            if ($this !== false && $this.parent().length) {
                if (+$this.parent().find('.js-count-calculator-indicator').text() > 1) {
                    flag_calc_count = true;
                    new_product['count'] = +$this.parent().find('.js-count-calculator-indicator').text();
                }
            }


            if (!arr_added_products.length) {
                arr_added_products.push(new_product);
            } else {
                for (let e = 0; e < arr_added_products.length; e++) {
                    if (arr_added_products[e]['id'] !== undefined && arr_added_products[e]['id'] === +product_id) {
                        arr_added_products[e]['count'] = (!flag_calc_count) ? +arr_added_products[e]['count'] + 1 : new_product['count'];

                        flag_change_nmb_added = false;

                        break;
                    } else {
                        if (e === arr_added_products.length - 1) {
                            arr_added_products.push(new_product);

                            break;
                        }
                    }
                }
            }

            localStorage.setItem('added_products', JSON.stringify(arr_added_products));

            return flag_change_nmb_added;
        }
    }
    //Package

    //Selected
    let selected_arr      = ( localStorage.getItem('selected_products') !== null && localStorage.getItem('selected_products') !== '[]' ) ? JSON.parse(localStorage.getItem('selected_products')) : false,
        select_products   = new Map(),
        selected_products = [],
        selected_number   = 0;

    checkSelectedProducts();

    setIndicatorSelectedProducts();

    $(document).on('click', '.js-product-btn-like', function () {
        let product_id = (!$(this).hasClass('card-section-right__btn-like')) ? $(this).parent().data('product-id') :  $(this).parents('.card-section-right').data('product-id');

        $(this).toggleClass('btn-like--selected');

        if (!select_products.has(product_id)) {
            select_products.set(product_id, true);
        } else {
            select_products.delete(product_id);
        }

        selected_arr = Array.from(select_products);

        selected_products = [];
        for (let i = 0; i < selected_arr.length; i++) {
            if (selected_arr[i][0] !== null) selected_products.push(selected_arr[i][0]);
        }

        localStorage.setItem('selected_products', JSON.stringify(selected_products));
        setIndicatorSelectedProducts();
    });

    drawSelectedProducts();

    function checkSelectedProducts() {
        if (selected_arr) {
            let selected_products_map = [],
                new_obj = {};

            for (let e = 0; e < selected_arr.length; e++) {
                new_obj = {};

                new_obj.key   = selected_arr[e];
                new_obj.value = true;

                selected_products_map.push(new_obj);
            }

            select_products = new Map( selected_products_map.map(i => [i.key, i.value]) );
        }
    }

    function setIndicatorSelectedProducts() {
        let class_products_selected_count        = '.header__section-top-right-list-item--selected',
            class_products_selected_count_mobile = '.head-nav-navigation__item--selected';

        selected_number = ( localStorage.getItem('selected_products') !== null && localStorage.getItem('selected_products') !== '') ? JSON.parse( localStorage.getItem('selected_products') ) : [];

        if (selected_number.length) {
            if (!$(class_products_selected_count + ' span').length) {
                $(class_products_selected_count + ' a, ' + class_products_selected_count_mobile + ' a').append('<span></span>');
            }

            $(class_products_selected_count + ' span, ' + class_products_selected_count_mobile + ' span').text( (selected_number.length < 100 ) ? selected_number.length : '99+' );
            $(class_products_selected_count + ', ' + class_products_selected_count_mobile).removeClass('checked-position-top');
        } else {
            $(class_products_selected_count + ' span,' + class_products_selected_count_mobile + ' span').remove();
            $(class_products_selected_count + ', ' + class_products_selected_count_mobile).addClass('checked-position-top');
        }
    }

    function drawSelectedProducts() {
        for (let p = 0; p < selected_number.length; p++) {
            $("[data-product-id='" + selected_number[p] + "']").find('.js-product-btn-like').addClass('btn-like--selected');
        }
    }
    //Selected
    //END: Header Menu

    //START: Form Select modified libraries
    var id_catalog_form_sort              = '#js-catalog-form-sort';
    var id_catalog_form_holidays          = '#js-catalog-sort-holidays';
    var class_elements_select             = '.js-select';
    var class_select_label                = '.selectLabel';
    var tag_name_option                   = 'option';
    var class_name_selected_option        = 'options';
    var class_selected_option             = '.' + class_name_selected_option;
    var class_name_selected_option_active = class_name_selected_option + '-active';

    var defaultSelectBox = $(id_catalog_form_sort + ' ' + class_elements_select);
    var numOfOptions = defaultSelectBox.children(tag_name_option).length;

    var holidaysSelectBox = $(id_catalog_form_holidays + ' ' + class_elements_select);
    var holidaysNumOfOptions = holidaysSelectBox.children(tag_name_option).length;

    // hide select tag
    $(class_elements_select).addClass('s-hidden');

    // wrapping default selectbox into custom select block
    $(class_elements_select).wrap('<div class="cusSelBlock"></div>');

    // creating custom select div
    $(class_elements_select).after('<div class="selectLabel"></div>');

    // getting default select box selected value
    $(class_elements_select).each(function () {
        $(this).next().text( $(this).children(tag_name_option).eq(0).text() );
    });

    // appending options to custom un-ordered list tag
    //TODO: Make this script universal
    var cusList = $('<ul/>', { 'class': 'options'} ).insertAfter($(id_catalog_form_sort + ' ' + class_select_label));
    var cusListHolidays = $('<ul/>', { 'class': 'options'} ).insertAfter($(id_catalog_form_holidays + ' ' + class_select_label));

    // generating custom list items
    for(var i = 0; i < numOfOptions; i++) {
        $('<li/>', {
            text: defaultSelectBox.children(tag_name_option).eq(i).text(),
            rel: defaultSelectBox.children(tag_name_option).eq(i).val()
        }).appendTo(cusList);
    }

    for(var o = 0; o < holidaysNumOfOptions; o++) {
        $('<li/>', {
            text: holidaysSelectBox.children(tag_name_option).eq(o).text(),
            rel: holidaysSelectBox.children(tag_name_option).eq(o).val()
        }).appendTo(cusListHolidays);
    }
    //TODO: Make this script universal

    // open-list and close-list items functions
    // function openList() {
    //     $('.options').addClass('options-active');
    // }
    //
    // function closeList() {
    //     $('.options').removeClass('options-active');
    // }

    function openList($this = false) {
        if ($this) $this.next().addClass(class_name_selected_option_active);
    }

    function closeList($this = false, is_select_wrapper = false) {
        if ($this && !is_select_wrapper) {
            $this.parent().removeClass(class_name_selected_option_active);
        } else {
            if (!is_select_wrapper) {
                $('.' + class_name_selected_option).removeClass(class_name_selected_option_active);
            } else $this.next().removeClass(class_name_selected_option_active);
        }
    }

    // click event functions
    $(class_select_label).click(function () {
        $(this).toggleClass('active');
        if( $(this).hasClass('active') ) {
            openList($(this));
            focusItems();
        }
        else {
            closeList($(this), true);
        }
    });

    $("." + class_name_selected_option + " li").on('keypress click', function(e) {
        let option_text = $(this).text();
        e.preventDefault();

        // $('.options li').siblings().removeClass();
        $(this).parent().children().siblings().removeClass();
        closeList($(this));

        $(this).parent().prev().removeClass('active');
        $(this).parent().prev().text(option_text);

        // defaultSelectBox.val($(this).text());
        $(this).parents('.cusSelBlock').children(class_elements_select).val(option_text);

        $('.selected-item p span').text($(class_select_label).text());
    });


    function focusItems() {
        $(class_selected_option).on('focus', 'li', function() {
            let $this = $(this);
            $this.addClass('active').siblings().removeClass();
        }).on('keydown', 'li', function(e) {
            let $this = $(this);
            if (e.keyCode == 40) {
                $this.next().focus();
                return false;
            } else if (e.keyCode == 38) {
                $this.prev().focus();
                return false;
            }
        }).find('li').first().focus();
    }

    //My script
    changeOptionActive();

    $(document).on('click', '.options li', function () {
        changeOptionActive($(this));
    });

    function changeOptionActive($this = false) {
        if ($this) {
            $this.parent().children().removeClass('active');

            $this.addClass('active');
        } else $('.options li' + ':first-of-type').addClass('active');
    }

    $(document).on('click', '#js-catalog-reset-sort', function () { resetSortSelects(); });

    $(document).on('dblclick', '*', function () {
        closeList();

        $(class_select_label).removeClass('active');
    });

    function resetSortSelects(is_sort_holidays = false) {
        let list_elements_select          = ['#js-catalog-form-sort ' + class_elements_select, '#js-catalog-sort-holidays ' + class_elements_select],
            location_select_sort          = '.catalog-head__section-right',
            location_select_sort_holidays = '.current-sorts-tab__form-holidays',
            current_select_sort           = (is_sort_holidays) ? location_select_sort_holidays : location_select_sort;

        $(current_select_sort + ' ' + class_selected_option + ' li').siblings().removeClass();

        $(current_select_sort + ' ' + class_selected_option + ' li:first-child').addClass('active');

        $(current_select_sort + ' ' + class_select_label).text($(current_select_sort + ' ' + class_selected_option + ' li:first-child').text());
        $( (is_sort_holidays) ? list_elements_select[1] : list_elements_select[0] ).val( $(current_select_sort + ' ' + class_selected_option + ' li:first-child').text() );

        $(current_select_sort + ' .selected-item p span').text( $(current_select_sort + ' ' + class_select_label).text() );
    }
    //My script
    //END: Form Select modified libraries

    //START: Catalog sorts
    $(document).on('click', '.js-catalog-sorts-tab-btn', function () {
        let status_tab_hidden = $(this).parents('.current-sorts-tab').attr('data-hidden');

        $(this).parents('.current-sorts-tab').attr( 'data-hidden', (status_tab_hidden !== 'false') ? false : true );
    });

    //CURRENT SORTS CONTENT
    $(document).on('click', '#js-catalog-reset-sorts', function () {
        resetCurrentPrice();
        resetCurrentHolidays();
    });

        //PRICE
        let id_catalog_range_price          = '#js-catalog-range-price',
            default_value_sort_price        = normalizePriceValue($(id_catalog_range_price).data('from')) + ' - ' + normalizePriceValue($(id_catalog_range_price).data('to')) + '₴',
            $range_catalog_price            = $(id_catalog_range_price),
            id_catalog_currrent_sorst_price = '#js-catalog-current-sort-price';

        changeCurrentPrice();

        $(document).on('click', '#js-catalog-reset-sort-price', resetCurrentPrice);

        $('#js-catalog-min-price, #js-catalog-max-price').on('keyup change', function () {
            let array_symbols = $(this).val().split('');

            if ( $.isNumeric(array_symbols[array_symbols.length - 1]) ) changeCurrentPrice();

            $(this).val( normalizePriceValue($(this).val()) );
        });

        // $(document).on('mouseleave change click move', '.irs-handle', changeCurrentPrice);

        function changeCurrentPrice() {
            if ($('.catalog-sorts-current__content-item--price').length) {
                $(id_catalog_currrent_sorst_price).addClass('hide');

                $(id_catalog_currrent_sorst_price).text(
                    normalizePriceValue( $('#js-catalog-min-price').val() ) + ' - ' + normalizePriceValue($('#js-catalog-max-price').val() ) + '₴'
                );

                setTimeout(function () {
                    $(id_catalog_currrent_sorst_price).removeClass('hide');
                }, 500);
            }
        }

        function normalizePriceValue(value_price = false) {
            let array_price   = value_price.toString().split(''),
                normal_value  = '';

            if (value_price) {
                for (let i = 0; i < array_price.length; i++) {
                    if ($.isNumeric(array_price[i])) {
                        if (array_price.length === 4 && i === 1) {
                            normal_value += ' ';
                        } else if (array_price.length === 5 && i === 2) {
                            normal_value += ' ';
                        } else {
                            if (array_price.length === 6 && i === 3) {
                                normal_value += ' ';
                            }
                        }

                        normal_value += array_price[i];
                    }
                }

                return normal_value;
            }
        }

        //TODO: I will include the range script from name 'The event reset price'
        function resetCurrentPrice() {
            if ($(id_catalog_currrent_sorst_price).text() !== default_value_sort_price) changeCurrentPrice();
        }
        //PRICE

        //HOLIDAYS
        let default_value_sort_holidays      = $('.current-sorts-tab__form-holidays' + ' ' + class_selected_option + ' li:first-child').text(),
            id_catalog_current_sort_holidays = '#js-catalog-current-sort-holidays';

        changeCurrentHolidays();

        $(document).on('click', '#js-catalog-reset-sort-holidays', resetCurrentHolidays);

        $(document).on('click', '#js-catalog-sort-holidays .cusSelBlock .options li', changeCurrentHolidays);

        function changeCurrentHolidays() {
            if ($('.catalog-sorts-current__content-item--holidays').length) {
                $(id_catalog_current_sort_holidays).addClass('hide');

                setTimeout(function () {
                    $(id_catalog_current_sort_holidays).text( $('#js-catalog-sort-holidays' + ' ' + class_select_label).text() );
                }, 500);

                setTimeout(function () {
                    $(id_catalog_current_sort_holidays).removeClass('hide');
                }, 650);
            }
        }

        function resetCurrentHolidays() {
            if ($(id_catalog_current_sort_holidays).text() !== default_value_sort_holidays) {
                resetSortSelects(true);

                changeCurrentHolidays();
            }
        }
        //HOLIDAYS
    //CURRENT SORTS CONTENT

    //PRICE
    //TODO: I will include my script from the libraries rangeSlider
    //PRICE

    //COLOR
    let class_current_sort_tab_colors = '.current-sorts-tab__colors';

    $('.colors-list li:first-child').addClass('active');

    $('.colors-list .js-sort-colors-item').each(function () {
        $(this).css('background-color', $(this).data('color') );
    });

    $(document).on('click', '.colors-list li', function () {
        $('.colors-list li').removeClass('active');

        $(this).addClass('active');
    });
    //COLOR
    //END: Catalog sorts

    //START: Pagination
    $(document).on('click', '.catalog-head__section-left .pagination__item', function () {
        $('.catalog-head__section-left .pagination__item').removeClass('pagination__item--active');

        $(this).addClass('pagination__item--active');
    });

    $(document).on('click', '.catalog-products-grid__pagination .pagination__item, .articles-grid__pagination .pagination__item', function () {
        if (!$(this).hasClass('pagination__item--dots')) {
            $('.catalog-products-grid__pagination .pagination__item, .articles-grid__pagination .pagination__item').removeClass('pagination__item--active');

            $(this).addClass('pagination__item--active');
        }
    });
    //END: Pagination

    //START: Card product
    let flag_change_product_image = true,
        id_card_product_image     = '#js-card-product-parent-image';

    $(document).on('click', '.js-card-product-image', function () {
        let current_product_image = $(this).attr('src');

        if ( $(id_card_product_image).attr('src') !== current_product_image ) {
            if (flag_change_product_image) {
                flag_change_product_image = false;

                $(id_card_product_image).addClass('hide');

                setTimeout(function () {
                    $(id_card_product_image).attr('src', current_product_image);
                }, 250);

                setTimeout(function () {
                    $(id_card_product_image).removeClass('hide');

                    flag_change_product_image = true;
                }, 500);
            }
        }
    });

    //Count Calculator
    let flag_change_count_number = true;

    $(document).on('click', '.js-count-calculator-btn-minus, .js-count-calculator-btn-plus', function () {
        let current_product_id = false,
            is_btn_minus      = $(this).hasClass('js-count-calculator-btn-minus'),
            $indicator_number = ( is_btn_minus ) ? $(this).next() : $(this).prev(),
            count_number      = +$indicator_number.text(),
            result_count      = ( !is_btn_minus ) ? ++count_number : --count_number,
            max_count_product = $(this).parent().find('.js-count-calculator-indicator').data('max-count');

        if ($(this).parents('.card-section-right').length) {
            current_product_id = $(this).parents('.card-section-right').data('product-id');
        } else if ($(this).parents('.basket-products-list__item').length) {
            current_product_id = $(this).parents('.basket-products-list__item').data('product-id');
        } else {
            current_product_id = $(this).parents('.product-item').data('product-id');
        }

        if (count_number > 0 && flag_change_count_number) {
            flag_change_count_number = false;

            if (result_count <= +max_count_product) {
                $indicator_number.addClass('hide');

                setTimeout(function () {
                    $indicator_number.text(result_count);
                }, 200);

                setTimeout(function () {
                    $indicator_number.removeClass('hide');
                }, 350);

                changeCountProduct(current_product_id, result_count);
            }

            if (max_count_product !== undefined) getCheckActiveBtnBuyCard(+max_count_product, +current_product_id);

            setTimeout(function () {
                flag_change_count_number = true;
            }, 350);

            changeControllerBtnCount(result_count, +max_count_product, $(this), is_btn_minus);
        }
    });

    function changeCountProduct(product_id = false, count_product = false, is_minus_count = false) {
        let arr_added_products = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [];

        if (product_id !== false && arr_added_products.length && count_product !== false) {
            if ( (count_product > 1 && is_minus_count) || (count_product >= 1 && !is_minus_count) ) {
                for (let e = 0; e < arr_added_products.length; e++) {
                    if (arr_added_products[e]['id'] === +product_id) {
                        arr_added_products[e]['count'] = count_product;

                        break;
                    }
                }
            }

            localStorage.setItem('added_products', JSON.stringify(arr_added_products));
        }
    }

    if ($('.card-section-right').length) {
        setTimeout(function () {
            changeControllerBtnCount(+$('.js-count-calculator-indicator').text(), +$('.js-count-calculator-indicator').data('max-count'));
        }, 500);
    }

    function changeControllerBtnCount(current_count = false, max_count = false, $_this = false, is_btn_minus = false) {
        console.log('current_count', current_count, 'max_count', max_count, 'is_btn_minus', is_btn_minus, '$_this', $_this);

        if (current_count !== false && max_count !== false && $_this !== false) {
            if ($_this.parents('.product-count-calculator').find('.js-count-calculator-btn-plus').attr('disabled') !== undefined) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-plus').attr('disabled', false);
            if ($_this.parents('.product-count-calculator').find('.js-count-calculator-btn-minus').attr('disabled') !== undefined) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-minus').attr('disabled', false);

            if (current_count === 1) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-minus').attr('disabled', true);
            if (current_count === max_count) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-plus').attr('disabled', true);

            // if (is_btn_minus) {
            //     if (current_count > 1) {
            //         if ($_this.attr('disabled') !== undefined) $_this.attr('disabled', false);
            //
            //         if ($_this.parents('.product-count-calculator').find('.js-count-calculator-btn-plus').attr('disabled') !== undefined) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-plus').attr('disabled', false);
            //     } else {
            //         $_this.attr('disabled', true);
            //
            //         if (current_count === 1 && current_count < max_count)  {
            //             if ($_this.parents('.product-count-calculator').find('.js-count-calculator-btn-plus').attr('disabled') !== undefined) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-plus').attr('disabled', false);
            //         }
            //     }
            // } else {
            //     if (!is_btn_minus) {
            //         if (current_count < max_count) {
            //             if ($_this.attr('disabled') !== undefined) $_this.attr('disabled', false);
            //
            //             if ($_this.parents('.product-count-calculator').find('.js-count-calculator-btn-minus').attr('disabled') !== undefined) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-minus').attr('disabled', false);
            //         } else {
            //             $_this.attr('disabled', true);
            //
            //             if (current_count === max_count) {
            //                 if ($_this.parents('.product-count-calculator').find('.js-count-calculator-btn-minus').attr('disabled') !== undefined) $_this.parents('.product-count-calculator').find('.js-count-calculator-btn-minus').attr('disabled', false);
            //             }
            //         }
            //     }
            // }
        } else {
            if (current_count !== false && max_count !== false) {
                if ($('.card-section-right').length) {
                    if ($('.js-count-calculator-indicator').data('max-count') !== undefined) {
                        if (+$('.js-count-calculator-indicator').text() <= 1) $('.js-count-calculator-btn-minus').attr('disabled', true);
                        if (+$('.js-count-calculator-indicator').text() >= +$('.js-count-calculator-indicator').data('max-count')) $('.js-count-calculator-btn-plus').attr('disabled', true);
                    } else {
                        $('.js-count-calculator-btn-minus').attr('disabled', true);
                        $('.js-count-calculator-btn-plus').attr('disabled', true);
                    }
                }
            }
        }
    }
    //Count Calculator

    //Tabs
    if ( $.trim($('.js-card-tabs-controllers-btn[data-count]').data('count')) !== '' ) {
        $('#js-card-tabs-reviews').text( $('.js-card-tabs-controllers-btn[data-count]').data('count') );
    } else {
        $('#js-card-tabs-reviews').remove();
    }

    $(document).on('click', '.js-card-tabs-controllers-btn', function () {
        let current_tab_id = +$(this).data('tab-id');

        if ($(".js-card-tabs-content-item[data-tab-id='"+ current_tab_id  +"']").length) {
            $('.js-card-tabs-controllers-btn.card-tabs-controllers-list__item--active').removeClass('card-tabs-controllers-list__item--active');
            $(this).addClass('card-tabs-controllers-list__item--active');

            if ( !$(".js-card-tabs-content-item[data-tab-id='"+ current_tab_id  +"']").hasClass('card-tabs-content__item--active') ) {
                $('.js-card-tabs-content-item').addClass('hide');

                $('.js-card-tabs-content-item.card-tabs-content__item--active').removeClass('card-tabs-content__item--active');
                $(".js-card-tabs-content-item[data-tab-id='"+ current_tab_id  +"']").addClass('card-tabs-content__item--active');

                setTimeout(function () {
                    $('.js-card-tabs-content-item').removeClass('hide');
                }, 350);
            }
        }
    });
    //Tabs
    //END: Card product

    //START: Basket products page
    $(document).on('click', '.js-basket-product-btn-delete', function () {
        let $this                = $(this),
            array_products       = 0,
            $elem_added_products = $('.' + class_name_navigation__item_mobile + ' ' + element_products_attr + ', ' + '.' + class_name_navigation__item + ' a' + ' span[data-products]');

        $this.parents('li').hide(350);

        setTimeout(function () {
            $this.parents('li').remove();

            sum_all_prices = 0;

            getSumPrices();
        }, 400);

        if ( !isNaN(+$(this).parents('.basket-products-list__item').data('product-id')) ) {
            removeProduct(+$(this).parents('.basket-products-list__item').data('product-id'));

            array_products = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [];

            if ($('.' + class_name_navigation__item_mobile + ' ' + element_products_attr + ', ' + '.' + class_name_navigation__item + ' ' + element_products_attr).length) {
                if (array_products.length) {
                    $elem_added_products.text( (array_products.length < 100) ? array_products.length : '99+');
                } else $elem_added_products.remove();
            }
        }
    });

    function removeProduct(id = false) {
        if (id !== false) {
            let array_products     = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [],
                new_array_products = [];

            if (array_products.length) {
                new_array_products = array_products.filter(item => item['id'] !== id);

                localStorage.setItem('added_products', JSON.stringify(new_array_products));
            }
        }
    }

    let rule_change_price = true;

    $(document).on('click', '.js-count-calculator-btn-minus, .js-count-calculator-btn-plus', function () {
        let $product_price        = $(this).parents('.basket-products-list__item-section-left').next().find('.js-basket-product-price'),
            max_count_product     = +$(this).parents('.basket-products-list__item-section-left').find('.js-count-calculator-indicator').data('max-count'),
            is_btn_minus          = ( $(this).hasClass('js-count-calculator-btn-minus') ) ? true : false,
            $indicator_number     = ( is_btn_minus ) ? $(this).next() : $(this).prev(),
            current_product_count = +$indicator_number.text(),
            new_indicator_number  = ( is_btn_minus ) ? current_product_count - 1 : current_product_count + 1,
            new_price_product     = 0;

        if ($(this).parents('.basket-products-list__item-section-left').length) {
            max_count_product = +$(this).parents('.basket-products-list__item-section-left').find('.js-count-calculator-indicator').data('max-count');
        } else {
            max_count_product = +$(this).parent().find('.js-count-calculator-indicator').data('max-count');
        }

        new_price_product = new_indicator_number * +$product_price.data('price');

        if (new_price_product > 0 && rule_change_price) {
            if ( ((new_indicator_number <= max_count_product) && is_btn_minus) || ((new_indicator_number < max_count_product + 1) && !is_btn_minus) ) {
                rule_change_price = false;

                $product_price.addClass('hide');

                setTimeout(function () {
                    $product_price.text( (new_price_product)  + '₴' );
                }, 300);

                setTimeout(function () {
                    $product_price.removeClass('hide');

                    if ( location.href.indexOf('basket') > -1 ) {
                        sum_all_prices = 0;

                        getSumPrices();
                    }

                    rule_change_price = true;
                }, 350);
            }
        }
    });


    let sum_all_prices    = 0,
        id_form_pay_price = '#js-form-pay-price';

    getSumPrices();

    function getSumPrices(received_sum_all_prices = false) {
        sum_all_prices = 0;

        setTimeout(function () {
            if ($('.js-basket-product-price').length) {
                if (!received_sum_all_prices) {
                    $('.js-basket-product-price').each(function () {
                        sum_all_prices += ( +$.trim($(this).parents('.basket-products-list__item').find('.js-count-calculator-indicator').text()) * +$.trim(Math.round( $(this).data('price')) ) );
                    });
                } else {
                    sum_all_prices = received_sum_all_prices;
                }

                $(id_form_pay_price).addClass('hide');

                setTimeout(function () {
                    $(id_form_pay_price).text( normalizePriceValue(sum_all_prices) );
                }, 300);

                setTimeout(function () {
                    $(id_form_pay_price).removeClass('hide');

                    $(id_form_pay_price).text( normalizePriceValue(sum_all_prices) );
                }, 350);
            }
        }, 500);
    }

    $(document).on('click', '#js-basket-products-btn-reset-all', function () {
        let rule_change_sum_all_prices = false,
            arr_added_products         = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [];

        if (arr_added_products.length) {
            for (let e = 0; e < arr_added_products.length; e++) {
                arr_added_products[e]['count'] = 1;
            }

            localStorage.setItem('added_products', JSON.stringify(arr_added_products));
        }

        $('.js-count-calculator-indicator').each(function () {
            if (+$(this).text() > 1) {
                rule_change_sum_all_prices = true;
            }
        });

        if (rule_change_sum_all_prices) {
            $('.js-count-calculator-indicator').each(function () {
                let $this                      = $(this),
                    $price_of_the_product      = $this.parents('.basket-products-list__item-section-left').next().find('.js-basket-product-price'),
                    $product_body_section_left = $this.parents('.basket-products-list__item-section-left');


                $this.addClass('hide');
                $price_of_the_product.addClass('hide');

                setTimeout(function () {
                    $this.text(1);

                    if ($product_body_section_left.find('.js-count-calculator-btn-minus').attr('disabled') === undefined) $product_body_section_left.find('.js-count-calculator-btn-minus').attr('disabled', true);

                    if ( ($this.data('max-count') !== undefined) && (+$this.data('max-count') <= 1) ) {
                        if ($product_body_section_left.find('.js-count-calculator-btn-plus').attr('disabled') === undefined) $product_body_section_left.find('.js-count-calculator-btn-plus').attr('disabled', true);
                    }

                    $price_of_the_product.text( $price_of_the_product.data('price') + '₴' );

                    $this.prev().click();
                }, 300);

                setTimeout(function () {
                    $this.removeClass('hide');
                    $price_of_the_product.removeClass('hide');

                    if ( location.href.indexOf('basket') > -1 ) {
                        sum_all_prices = 0;
                    }
                }, 350);
            });

            getSumPrices();
        }
    });

    $(document).on('click', '.basket-section-pay__form-checkbox label', function () {
       $(this).toggleClass('no-valid');
    });
    //END: Basket products page
});
