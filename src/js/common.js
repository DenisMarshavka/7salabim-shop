import $ from 'jquery';
import Swiper from 'swiper';

$(document).ready(function () {
    //START: Header menu
    $(document).on('click', '.header-menu__item', function () {
        $('.header-menu__item').removeClass('header-menu__item--active');
        $(this).addClass('header-menu__item--active');
    });

    $(document).on('click', '.header-section-top__nav-language-item', function () {
        $('.header-section-top__nav-language-item').removeClass('header-section-top__nav-language-item--active');
        $(this).addClass('header-section-top__nav-language-item--active');
    });

    //START: Form
    $(document).on('mouseover', '.header-section-top__nav-item button i', function () {
        $(this).parents('ul').addClass('header-section-top__nav--form-active');
    });

    $(document).on('mouseleave', '.header-section-top__nav-item form', function () {
        $(this).parents('ul').removeClass('header-section-top__nav--form-active');
    });

    $(document).on('click', '#btn-clear-search-input', function () {
        $('#header-search').val('');
    });
    //END: Form
    //END: Header menu

    //START: Slider Articles
    $('#js-slider-articles').slick({
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        prevArrow: $('#js-slider-articles-btn-prev'),
        nextArrow: $('#js-slider-articles-btn-next'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    //END: Slider Articles
});