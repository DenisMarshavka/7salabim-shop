(function($) {
    $.fn.sumSale = function (option) {
        var $this   = $(this),
            old_price,
            new_price,
            one_percent,
            percent_between_prices;

        $.extend(this, option);

        old_price = this.oldPrice;
        new_price = this.newPrice;

        one_percent = old_price / 100;
        percent_between_prices = new_price / one_percent;

        if (old_price > new_price) $("<div class='" + this.classNameSale + "'><span>" + Math.round(100 - percent_between_prices) + "</span></div>").insertAfter($this.find('figure.small-alert-info'));
    }
})(window.jQuery);