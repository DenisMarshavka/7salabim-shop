import $ from 'jquery';
import Swiper from "swiper";

$(document).ready(function () {
    //START: Specifications
    //Open/Close Pop-Up
    function changePopup(is_open = true) {
        $('html, body').css((is_open) ? {overflowY: 'hidden', position: 'fixed'} :{overflowY: 'auto', position: 'relative'});
    }

    // let offset_between_mobile_menu = 0,
    //     with_mobile_menu_item      = 0;
    //
    // if (window.outerWidth <= 767) {
    //     offset_between_mobile_menu = window.outerWidth - $('.header-menu.swiper-wrapper').width();
    //     with_mobile_menu_item = $('.header-menu__item').eq(0).width() * 2;
    //
    //     setTimeout(function () {
    //         $('.header-menu.swiper-wrapper').css({marginLeft: (offset_between_mobile_menu + with_mobile_menu_item) + 'px'});
    //     },  1500);
    //     // console.log(offset_between_mobile_menu - with_mobile_menu_item);
    // }
    //Open/Close Pop-Up
    //END: Specifications

    //START: Form
    let set_time_out_check_value_search,
        count_input_search_empty = 0;

    $(document).on('mouseleave', '.header-head form', function () {
        $(this).parents('ul').removeClass('header-head--form-active');

        $('.header-search').val('');

        clearTimeOutFormSearch();
    });

    $(document).on('click', '.btn-clear-search-input', function () {
        $('.header-search').val('');

        clearTimeOutFormSearch();

        checkFormSearchValue();
    });

    if (window.outerWidth <= 767) {
        $(document).on('click', '.header-head form button', function (event) {
            if ( !$.trim( $('.header-head__form-input .header-search').val() ).length ) {
                event.preventDefault();
                event.stopPropagation();

                $(this).parents('ul').addClass('header-head--form-active');

                checkFormSearchValue();
            }
        });

        $(document).on('keyup', '.header-head__form-input .header-search', function () {
            clearTimeout(set_time_out_check_value_search);

            checkFormSearchValue();
        });
    }

    function checkFormSearchValue() {
        if (count_input_search_empty < 4) {
            set_time_out_check_value_search = setTimeout(function () {
                ++count_input_search_empty;

                checkFormSearchValue();
            }, 2000);

        } else {
            if ( $.trim( $('.header-head__form-input .header-search').val() ) === '') {
                if ($('.header-head').hasClass('header-head--form-active')) $('.header-head').removeClass('header-head--form-active');

                clearTimeout(set_time_out_check_value_search);
            }

            count_input_search_empty = 0;
        }
    }

    function clearTimeOutFormSearch() {
        clearTimeout(set_time_out_check_value_search);
        count_input_search_empty = 0;
    }
    //END: Form

    //START: Header Menu
    $(document).on('click', '.header-head__language-item', function () {
        $('.header-head__language-item').removeClass('header-head__language-item--active');
        $(this).addClass('header-head__language-item--active');
    });

    if (window.outerWidth <= 767) {
        var mySwiper = new Swiper('#js-header-menu', {
            slidesPerView: 5,
            spaceBetween: 25,
            freeMode: true,
            // centeredSlides: true,
            navigation: {
                nextEl: '.header-menu-btn',
            },
        });
    }
    //END: Header Menu

    //START: Header submenu
    let class_name_popup_submenu = 'header-submenu-popup';

    $(document).on('click', '.header-menu__item--dropdown', function () {
        if (window.outerWidth < 768) {
            let submenu_html = $(this).children('.header-menu__item-submenu-list').children('.header-submenu').html();

            $('.' + class_name_popup_submenu + ' .header-submenu').html(submenu_html);
            $('.' + class_name_popup_submenu + ' .header-submenu').parents().toggleClass('header-submenu-popup--active');

            changePopup();
        }
    });

    $(document).on('click', 'button#js-submenu-popup-btn-close', function () {
        if (window.outerWidth < 768) {
            $('.' + class_name_popup_submenu).toggleClass('header-submenu-popup--active');

            changePopup(false);
        }
    });
    //END: Header submenu

    //START: Catalog-page
    $(document).on('click', '#js-catalog-content-btn-mobile', function () {
        $(this).parent().toggleClass('open');

        if (!$(this).parent().hasClass('open')) {
            $(this).text( $(this).data('open-text') );
        } else {
            $(this).text( $(this).data('close-text') );
        }
    });
    //END: Catalog-page
});