import AppService from './modules/app.service';
import {config} from './modules/config';
// import './modules/header.conponent'

//LIBRARIES CSS
import './assets/css/libraries/slick.css';
import './assets/css/libraries/swiper.min.css';
import './assets/css/libraries/owl.carousel.css';
import './assets/css/libraries/ion.rangeSlider.min.css';
import './assets/css/libraries/font-aweasome-all.min.css';
import './assets/css/libraries/animate.css';

//LIBRARIES JS
import './js/libraries/slick.min';
import './js/libraries/swiper.min';
// import './js/libraries/fontaweasome-all.min';
// import './js/libraries/ion.rangeSlider.min';
// import './js/libraries/owl.carousel.min';

//FONTS
import './assets/css/material-design-iconic-font.css';

//SCRIPTS
import './js/universal-scripts';
import './js/common';
import './js/mobile-scripts';

//STYLES
import './assets/css/main.css';
import './assets/sass/main.sass';


